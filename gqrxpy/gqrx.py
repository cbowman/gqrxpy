"""
gqrxpy.gqrx

Control GQRX via TCP telnet connection

Craig Bowman, K7FLG - 2018
"""
import sys
import telnetlib
import time
import logging
__version__ = "0.0.1"

class Receiver:
    """Send commands and receive output"""
    def __init__(self, address="127.0.0.1", port="7356"):
        self.address = address
        self.port = port
        try:
            self.conn = telnetlib.Telnet(self.address, self.port)
        except ConnectionRefusedError:
            sys.exit("Failed to connect to gqrx telnet server. Is it running?")

    def set_frequency(self, frequency):
        """Set receive frequency (HZ)"""
        self.conn.write(("F " + frequency + "\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        if "RPRT 0" not in result:
            logging.warning("Failed to set frequency")

    def set_mode(self, mode):
        """
        Set mode
        options are OFF RAW AM FM WFM WFM_ST WFM_ST_OIRT LSB USB CW CWU CWR CWL
        Optional third argument: Passband in Hz
        """
        self.conn.write(("M " + mode + "\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        if "RPRT 0" not in result:
            logging.warning("Failed to set mode")

    def set_squelch(self, squelch):
        """Set squelch (dBFS)"""
        self.conn.write(("L " + squelch + "\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        if "RPRT 0" not in result:
            logging.warning("Failed to set squelch")

    def start_recording(self):
        """Start recording to predefined directory"""
        self.conn.write(("AOS\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        if "RPRT 0" not in result:
            logging.warning("Failed to start recording")

    def stop_recording(self):
        """Stop recording"""
        self.conn.write(("LOS\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        if "RPRT 0" not in result:
            logging.warning("Failed to stop recording")

    def get_signal_strength(self):
        """Get signal strength of current frequency (dBFS)"""
        self.conn.write(("l STRENGTH\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        return result

    def get_squelch(self):
        """Get signal strength of current frequency (dBFS)"""
        self.conn.write(("l SQL\n").encode('ascii'))
        result = self.conn.read_some().decode('ascii').strip()
        return result

class Scanner(Receiver):
    """Scanner (inherits receiver class)"""
    def __init__(self, pause_time=15, dwell_time=0.2, address="127.0.0.1", port="7356"):
        super(Scanner, self).__init__(address, port)
        self.pause_time = pause_time
        self.dwell_time = dwell_time

    def _monitor(self):
        """Monitor frequency for pause_time, or until signal drops"""
        drop_time = time.time() + self.pause_time
        while time.time() < drop_time:
            if float(self.get_signal_strength()) >= float(self.get_squelch()):
                time.sleep(1)
            else:
                break

    def bookmark_scan(self, bookmarks):
        """Scan a set of bookmarks"""
        try:
            while True:
                for bookmark in bookmarks:
                    self.set_frequency(str(bookmark["freq"]))
                    self.set_mode(bookmark["mode"])
                    time.sleep(self.dwell_time)
                    if float(self.get_signal_strength()) >= float(self.get_squelch()):
                        self._monitor()
        except KeyboardInterrupt:
            sys.exit('\n')

    def range_scan(self, start_freq, end_freq, step=10000):
        """Scan a frequency range"""
        freq = start_freq
        try:
            while True:
                self.set_frequency(str(freq))
                time.sleep(self.dwell_time)
                if float(self.get_signal_strength()) >= float(self.get_squelch()):
                    self._monitor()
                if int(freq) < int(end_freq):
                    freq = int(freq)
                    freq += step
                else:
                    freq = start_freq
        except KeyboardInterrupt:
            sys.exit('\n')
