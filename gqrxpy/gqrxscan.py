#!/usr/bin/env python3
"""
gqrxpy.gqrxscan.py

Command line interface for scanning with gqrx.py Scanner class

Craig Bowman, K7FLG - 2018
"""
import argparse
import csv
import sys
import os.path
from gqrxpy.gqrx import Scanner
__version__ = "0.0.1"

def read_bookmarks_file(bookmarks_file):
    """Get bookmarks from GQRX bookmarks-style CSV file and return dict of bookmarks"""
    bookmarks = []
    fieldnames = ["freq", "name", "mode", "bw", "tags"]
    translate_modes = {
        'Demod Off': 'OFF',
        'Raw I/Q': 'RAW',
        'AM': 'AM',
        'Narrow FM': 'FM',
        'WFM (mono)': 'WFM',
        'WFM (stereo)': 'WFM_ST',
        'WFM (oirt)': 'WFM_ST_OIRT',
        'CW-U': 'CWU',
        'CW-L': 'CWL',
    }

    reader = csv.DictReader(
        filter(lambda row: row[0] != '#', bookmarks_file),
        fieldnames, skipinitialspace=True, delimiter=';'
        )
    reader = (dict((n, v.strip()) for n, v in row.items() if v) for row in reader)
    for line in reader:
        if len(line.keys()) == 5:
            bookmarks.append(line)
        else:
            continue
    for bookmark in bookmarks:
        if bookmark["mode"] in translate_modes.keys():
            bookmark["mode"] = translate_modes[bookmark["mode"]]
    return bookmarks

def parser_setup(gqrxdir):
    """Set up argument parser"""
    description = "Scan a frequency range or bookmarks file in GQRX"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--scanrange', '-r', nargs=2, type=int,
                        help='Scan a range of frequencies')
    parser.add_argument('--step', '-s', nargs='?', type=int, default=10000,
                        help='Step in hertz for range scan')
    parser.add_argument('--dwell', '-d', nargs='?', type=float, default=0.2,
                        help='Time to wait before reading frequency')
    parser.add_argument('--pause', '-p', nargs='?', type=int, default=15,
                        help='Time to pause on a busy frequency')
    parser.add_argument('--file', '-f', nargs='?', default=(gqrxdir + 'bookmarks.csv'),
                        type=argparse.FileType('r'),
                        help='CSV file with bookmarks to be scanned')
    parser.add_argument('--tags', '-t', nargs='+',
                        help='Tags to scan in bookmarks file')
    parser.add_argument('--port', '-P', nargs='?', default='7356',
                        help='receiver telnet port')
    parser.add_argument('--version', '-v', action='store_true',
                        help='Show version info')
    args = parser.parse_args()
    return args

def __main__():
    """Accept input and run scanner"""
    gqrxdir = os.path.expanduser('~/.config/gqrx/')
    args = parser_setup(gqrxdir)
    if args.version is True:
        print("gqrxpy version " + __version__)
        sys.exit()

    scanner = Scanner(pause_time=args.pause, dwell_time=args.dwell, port=str(args.port))
    if args.scanrange:
        start = str(args.scanrange[0])
        end = str(args.scanrange[1])
        scanner.range_scan(start, end, step=args.step)
    if args.file:
        bookmarks = []
        if args.tags:
            for bookmark in read_bookmarks_file(args.file):
                if bookmark["tags"] in args.tags:
                    bookmarks.append(bookmark)
        else:
            bookmarks = read_bookmarks_file(args.file)
        scanner.bookmark_scan(bookmarks)

if __name__ == "__main__":
    __main__()
