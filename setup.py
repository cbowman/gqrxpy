import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gqrxpy",
    version="0.0.1",
    author="Craig Bowman",
    author_email="cbbowman.cb@gmail.com",
    description="A simple scanner and control module for gqrx SDR receiver",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cbowman/gqrxpy/",
    packages=setuptools.find_packages(),
    scripts=['gqrxpy/gqrxscan.py'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
)
