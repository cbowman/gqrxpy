# gqrxpy
gqrxpy is a simple python3 control system for [gqrx](http://gqrx.dk/). It uses gqrx's telnet remote control
for sending commands, and includes a Scanner class for scanning bookmarks or frequency ranges.

## Requirements
  * gqrx Version 2.5.3 or higher, with [telnet control enabled](http://gqrx.dk/doc/remote-control)

## Scanner Usage
Use ```ctrl+c``` to stop any scan

### Bookmark scanning
By default, running gqrxscan.py will scan the bookmarks stored in _~/.config/gqrx/bookmarks.csv_:
```
~$ gqrxscan.py
```

If you want to use a different bookmarks CSV file, use the --file or -f option:
```
~$ gqrxscan.py -f ./custom_bookmarks.csv
```
This repository contains an example bookmarks file.

If your bookmarks file includes tags, you can limit the scan to any number of 
those tags in a space-separated list:
```
~$ gqrxscan.py -f ./custom_bookmarks.csv -t "Calling Freqs" GMRS FRS
```

### Range scanning
Instead of bookmarks, you can also scan a range of frequencies (specified in Hz) with the -r option:
```
~$ gqrxscan.py -r 144000000 148000000
```
and set a custom step size if you want (defaults to 10000 Hz)
```
~$ gqrxscan.py -r 144000000 148000000 -s 5000
```
Range scans will use the receiver settings in the gqrx Receiver pane (i.e., they will not change the mode
like a range scan)

#### Timing Options
By default, the scan will spend 0.2 seconds on each frequency, and hold a busy frequency
for 15 seconds or until it drops. If you want to set a different hold time, use ```-p <seconds>``` or
```--pause <seconds>```. If you want a longer dwell time before moving on from a quiet frequency,
specify it with ```-d <seconds>``` or ```--dwell <seconds>```. Since some receivers are slower to
change frequencies, the default may be too fast. If you notice that the scanner is not stopping on
busy frequencies, try setting a longer dwell time.

#### Telnet Port
If you aren't running the gqrx telnet control server at port 7356 (the detault),
you can specify an alternate port with ```-P <port>``` or ```--port <port>```
